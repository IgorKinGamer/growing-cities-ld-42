// colocarNaGradeIU(grade, objeto, linha, coluna, alinH, alinV)

var grade  = argument0;
var objeto = argument1;
var linha  = argument2 - 1;
var coluna = argument3 - 1;
var alinH  = argument4;
var alinV  = argument5;

grade.grade[#coluna, linha] = objeto;

objeto.IU_alinH = alinH;
objeto.IU_alinV = alinV;


// Posicionar
objeto.x = grade.x + grade.dxs[coluna] + objeto.IU_alinH * grade.larguras[|coluna];
objeto.y = grade.y + grade.dys[linha]  + objeto.IU_alinV * grade.alturas[|linha];

