// criarGradeIU([x, y,] alturas, larguras)
// alturas: lista de alturas das linhas
// larguras: lista de larguras das colunas

var _x;
var _y;
var alturas;
var larguras;

if (argument_count == 2)
{
    _x = 0;
    _y = 0;
    alturas  = argument[0];
    larguras = argument[1];
}
else if (argument_count == 4)
{
    _x       = argument[0];
    _y       = argument[1];
    alturas  = argument[2];
    larguras = argument[3];
}

var nLinhas  = ds_list_size(alturas);
var nColunas = ds_list_size(larguras);

var grade = instance_create(_x, _y, obj_IU_Grade);

grade.nLinhas  = nLinhas;
grade.alturas  = alturas;
grade.nColunas = nColunas;
grade.larguras = larguras;

grade.grade = ds_grid_create(nColunas, nLinhas);

grade.dxs[nColunas-1] = 0;
grade.dys[nLinhas-1]  = 0;

grade.dxs[0] = 0;
var c;
for (c = 1; c < nColunas; c++)
    grade.dxs[c] = grade.dxs[c-1] + larguras[|c-1];

grade.dys[0] = 0;
var l;
for (l = 1; l < nLinhas; l++)
    grade.dys[l] = grade.dys[l-1] + alturas[|l-1];

return grade;

