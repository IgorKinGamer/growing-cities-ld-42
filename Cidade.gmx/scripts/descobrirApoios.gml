// descobrirApoios()

//noObjeto(obj_Construcao);

var _py = (y / BLOCO) + altura;

// Se tem chão, não está apoiada em outras construções
for (var _px = px; _px < px + largura; _px++)
{
    var _chao = objetoNaGrade(_px, _py);
    if (_chao != noone && eDoTipo(_chao, obj_Bloco))
        exit;
}

for (var _px = px; _px < px + largura; _px++)
{
    var _apoio = objetoNaGrade(_px, _py);
    if (_apoio != noone && eDoTipo(_apoio, obj_Construcao))
    {
        ds_list_add(apoios, _apoio);
        // Pula o resto da construção (-1 porque tem o _px++)
        _px = _apoio.px + _apoio.largura - 1;
        
        // Coloca-se na lista de apoiadas dele
        ds_list_add(_apoio.apoiadas, id);
    }
}
