// moverConstrucao(constr)

//noObjeto(obj_Controle);

var constr = argument0;

with (constr)
{
    // Se algum apoio não está caindo, a construção também não está
    for (var i = 0; i < ds_list_size(apoios); i++)
    {
        if (!apoios[|i].marcada)
        {
            caindo = false;
            marcada = false;
            // Atualiza distribuição do peso
            ds_list_add(obj_Controle.recemParadas, id);
            ds_queue_enqueue(obj_Controle.basesRecemParadas, id);
            exit;
        }
    }
    
    // Se nem todos apoios foram processados, enfileira de volta
    for (var i = 0; i < ds_list_size(apoios); i++)
    {
        if (!apoios[|i].processada)
        {
            reenfileirar(id);
            // Pula o restante
            exit;
        }
    }
    
    processada = true;
    
    // Tira da grade (agora que sabe que está caindo)
    tirarDaGrade(id);
    
    
    // Verifica se encosta antes de movY
    verificarColisao();
    
    // Se parou, atualizará as relações irá para a grade
    if (!caindo)
        ds_list_add(obj_Controle.recemParadas, id);
    
    /* Move */
    
    // Se está caindo, move movY
    if (caindo)
        y += movY;
    // Senão, move com precisão (dy)
    else
        y = ((y div BLOCO) + dy) * BLOCO; // TODO pyParaY(...);
    
    
    // Trata as apoiadas
    for (var i = 0; i < ds_list_size(apoiadas); i++)
    {
        var _apoiada = apoiadas[|i];
        // Enfileira se ainda não foi
        enfileirar(_apoiada);
        // Herda o movimento
        _apoiada.movY = movY;
        _apoiada.velV = velV;
        if (!caindo) // Apoio (eu) parou
        {
            // Apoiada para também
            if (_apoiada.caindo) // Ainda estava caindo
            {
                _apoiada.caindo = false;
                _apoiada.dy = dy;
            }
            else // Já tinha parado
            {
                // Já tem dy, fica com o menor
                _apoiada.dy = min(_apoiada.dy, dy);
            }
        }
    }
}
