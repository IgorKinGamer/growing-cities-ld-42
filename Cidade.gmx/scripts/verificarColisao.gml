// verificarColisao()

//noObjeto(obj_Construcao);

// Fundo (base) da construção
var _yFundo = y + altura*BLOCO;
// Distância total da grade
var _distY;
if (caindo) // Ainda não encostou
    _distY = (_yFundo mod BLOCO) + movY;
else // Já encostou (tem dy)
    _distY = dy * BLOCO;

// Se é menor que BLOCO, não encosta em nada
// Se é maior ou igual a BLOCO, verifica
if (_distY >= BLOCO)
{
    var _pyFundo = _yFundo div BLOCO;
    // Verifica cada linha
    for (var i = 1; i*BLOCO <= _distY; i++)
    {
        var _pyVerif = _pyFundo + i;
        // Verifica cada bloco da linha
        for (var j = 0; j < largura; j++)
        {
            var _pxVerif = px + j;
            // Tem algo
            if (gradeOcupada(_pxVerif, _pyVerif))
            {
                ds_queue_enqueue(obj_Controle.basesRecemParadas, id);
                // Usa dy em vez de movY
                dy = i;
                movY = -1;
                // Encostou
                caindo = false;
                // TODO Rápido demais
                velV = 0;
                // Para de verificar
                exit;
            }
        }
    }
}
