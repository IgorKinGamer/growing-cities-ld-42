// atributosConstrucao(tipo: Objeto)

var tipo = argument0;

peso = obj_Jogo.pesos[?tipo];
resistencia = obj_Jogo.resistencias[?tipo];

switch (tipo)
{
    case obj_CasaDeMadeira:
        nome = obj_Config.txtCasaDeMadeira;
        minMoradores = 2;
        maxMoradores = 7;
        break;
        
    case obj_CasaDeConcreto:
        nome = obj_Config.txtCasaDeConcreto;
        minMoradores = 2;
        maxMoradores = 5;
        /*ds_list_add(listaDeSprites, spr_Teto, spr_Porta);
        ds_list_add(listaDeImagens, 0,        0);*/
        break;
        
    case obj_Predio:
        nome = obj_Config.txtPredio;
        minMoradores = 15;
        maxMoradores = 40;
        break;
        
    case obj_CasaDeDiamante:
        nome = obj_Config.txtCasaDeDiamante;
        minMoradores = 1;
        maxMoradores = 4;
        break;
}
