// eDoTipo(instância, tipo: Objeto)

var inst = argument0;
var tipo = argument1;

return inst.object_index == tipo
        || object_is_ancestor(inst.object_index, tipo);
