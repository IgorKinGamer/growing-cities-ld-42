// marcarComoCaindo(construção)

var constr = argument0;

with (constr)
{
    // Ainda não foi marcada como caindo
    if (!marcada) // Errado - caindo não implica já visitada
    {
        // Marca
        marcada = true;
        // TODO Tira da grade
        caindo = true;
        naFila = false;
        processada = false;
        // Visita cada apoiada
        for (var i = 0; i < ds_list_size(apoiadas); i++)
            marcarComoCaindo(apoiadas[|i]);
    }
}
