// configurarTextos(lingua: INGLES ou PORTUGUES)

var lingua = argument0;

txtTitulo = "Growing Cities";

if (lingua == PORTUGUES)
{
    with (obj_Config)
    {
        txtCliqueQualquer = "«Clique em qualquer lugar para começar»";
        
        txtNadaEmFoco = "«Nada em foco»";
        
        txtCasaDeMadeira = "Casa de madeira";
        txtCasaDeConcreto = "Casa de concreto";
        txtPredio = "Prédio";
        txtCasaDeDiamante = "Casa de diamante";
        
        txtPeso = "Peso";
        txtResist = "Resistência";
        txtPesoReceb = "Peso recebido";
        txtPesoTotal = "Peso total";
        txtMoradores = "Moradores";
        txtPopulacao = "População";
        txtObjetivo = "Objetivo";
        txtDestruidas = "Construções#destruídas";
        
        txtConstrAtual = "Construção atual";
        txtProxConstr = "Próxima construção";
        
        txtTempoRestante = "Tempo restante";
        
        txtBotaoEsquerdo = "«Botão esquerdo#para posicionar»"
        txtBotaoDireito = "«Botão direito#para descartar»"
        
        txtParabens = "Parabéns!";
        txtAtrasou = "Você está#atrasado!";
        txtDestruiu = "Você está#destruindo a cidade!";
        txtObrigado = "Obrigado#por jogar!##LD 42##Igor KinGamer";
        
        txtFala1_Ola =
                "Olá!"
                + "##Disseram que você é um ótimo"
                + "#especialista em planejamento urbano."
                + "##Nós estamos com um pequeno problema"
                + "#nas nossas cidades, nossas pequenas"
                + "#cidades..."
                + "##«Clique para ouvir mais»";
        txtFala1_Missao =
                "Esta cidade é bem pequena, como você"
                + "#pode ver."
                + "##Mas nós estamos recebendo muita gente,"
                + "#e precisamos acomodá-la."
                + "##Sua missão é posicionar construções"
                + "#suficientes para esse número de pessoas.";
        txtFala1_Regras =
                "Mas, por favor, nunca, jamais, se atrase."
                + "##E evite destruir construções, nossos"
                + "#recursos são limitados...";
        txtFala1_BoaSorte =
                "...Ficando sem espaço?"
                + "##Ah, você vai conseguir dar um jeito nisso."
                + "##Boa sorte!";
                
        txtFala2 =
                "As pessoas estão vindo rápido!";
                
        txtFala3 =
                "Esta cidade é tão estranha..."
                + "##Como atrai tanta gente?";
                
        txtFala4 =
                "Tem gente rica vindo pras nossas cidades!";
                
        txtFala5 =
                "Sua última missão!";
    }
}
else if (lingua == INGLES)
{
    with (obj_Config)
    {
        txtCliqueQualquer = "«Click anywhere to start»";
        
        txtNadaEmFoco = "«Nothing in focus»";
        
        txtCasaDeMadeira = "Wood house";
        txtCasaDeConcreto = "Concrete house";
        txtPredio = "Building";
        txtCasaDeDiamante = "Diamond house";
        
        txtPeso = "Weight";
        txtResist = "Resistance";
        txtPesoReceb = "Supported#weight";
        txtPesoTotal = "Total weight";
        txtMoradores = "Residents";
        txtPopulacao = "Population";
        txtObjetivo = "Goal";
        txtDestruidas = "Buildings#Destroyed";
        
        txtConstrAtual = "Current building";
        txtProxConstr = "...";
        
        txtTempoRestante = "Remaining time";
        
        txtBotaoEsquerdo = "«Left click#to place»"
        txtBotaoDireito = "«Right click#to discard»"
        
        txtParabens = "Congratulations!";
        txtAtrasou = "You're late!";
        txtDestruiu = "You're destroying#the city!";
        txtObrigado = "Thanks#for playing!##LD 42##Igor KinGamer";
        
        txtFala1_Ola =
                "Hi there!"
                + "##They've told me you're"
                + "#a urban planning expert."
                + "##We have a little problem in our"
                + "#cities, our little cities..."
                + "##«Click to hear more»";
        txtFala1_Missao =
                "This city is very small, as you can"
                + "#see."
                + "##But there's a lot of people coming,"
                + "#and we need to accommodate them."
                + "##Your mission is to place enough"
                + "#buildings for this number of people.";
        txtFala1_Regras =
                "But, please, do not ever get late."
                + "##And avoid breaking buildings, we"
                + "#have limited resources...";
        txtFala1_BoaSorte =
                "...Running out of space?"
                + "##Oh, you can overcome that."
                + "##Good luck!";
                
        txtFala2 =
                "People are coming fast!";
                
        txtFala3 =
                "This city is so weird..."
                + "##Why does it attract so many people?";
                
        txtFala4 =
                "There's rich people coming to our cities!";
                
        txtFala5 =
                "Your last mission!";
    }
}
