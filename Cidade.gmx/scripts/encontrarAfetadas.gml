// encontrarAfetadas(constr)

//noObjeto(obj_Controle);

var _constr = argument0;

// Ainda não está na lista
if (ds_list_find_index(afetadas, _constr) < 0)
{
    // Coloca-se na lista de afetadas
    ds_list_add(afetadas, _constr);

    // Coloca cada apoiada e cada apoio na lista de afetadas
    for (var i = 0; i < ds_list_size(_constr.apoios); i++)
        encontrarAfetadas(_constr.apoios[|i]);
    for (var i = 0; i < ds_list_size(_constr.apoiadas); i++)
        encontrarAfetadas(_constr.apoiadas[|i]);
}
