// atualizarPeso(constr)

var constr = argument0;

with (constr)
{
    // Ainda não foi atualizada
    if (!atualizada)
    {
        atualizada = true;
        pesoRecebido = 0;
        
        // Atualiza apoiadas
        for (var i = 0; i < ds_list_size(apoiadas); i++)
        {
            var _apoiada = apoiadas[|i];
            atualizarPeso(_apoiada);
            pesoRecebido += _apoiada.pesoEfetivo / ds_list_size(_apoiada.apoios);
        }
        // Atualiza o próprio peso
        pesoEfetivo = peso + pesoRecebido;
        
        // Peso demais: irá quebrar (se não está já quebrando)
        if (!quebrando && pesoRecebido > resistencia)
            ds_list_add(obj_Controle.listaDeQuebra, id);
    }
}
