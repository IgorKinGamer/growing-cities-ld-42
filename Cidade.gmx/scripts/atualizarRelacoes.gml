// atualizarRelacoes(constr)

var constr = argument0;

with (constr)
{
    // Remove-se da lista de apoioadas de seus apoios antigos
    for (var ap = 0; ap < ds_list_size(apoios); ap++)
    {
        var _apoio = apoios[|ap];
        ds_list_delete(
                _apoio.apoiadas,
                ds_list_find_index(_apoio.apoiadas, id)
        );
    }
    // Limpa sua lista de apoios
    ds_list_clear(apoios);
    // Descobre seus apoios atuais
    descobrirApoios();
}
